#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QColor>
#include <QPen>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = nullptr);

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;

    enum ShapeType{ Astroid,
                    Cycloid,
                    HuygensCycloid,
                    HypoCycloid,
                    Line,
                    Circle,
                    Ellipse,
                    Fancy,
                    Starfish,
                    Cloud1,
                    Cloud2
                  };

    void setBackgroundColor(QColor color) { mBackgroundColor = color; } // setter
    QColor backgroundColor() const { return mBackgroundColor; } // getter

    // Chainging from just a QColor to QPen object.
    // void setShapeColor(QColor color) { mShapeColor = color; }
    void setShapeColor(QColor color) { mPen.setColor(color); }
    QColor shapeColor() const { return mPen.color(); }

    void setShape(ShapeType shape) { mShape = shape; onShapeChanged(); }
    ShapeType shape() const { return mShape; }

    void setScale(float scale) { mScale = scale; repaint(); }
    float scale() const { return mScale; }

    void setIntervalLength(float interval) { mIntervalLength = interval; repaint(); }
    float intervalLength() const { return mIntervalLength; }

    void setStepCount(int count) { mStepCount = count; repaint(); }
    int stepCount() const { return mStepCount; }

protected:
    void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;

signals:

public slots:

private:
    QColor mBackgroundColor;
    // Chainging from just a QColor to QPen object.
    //QColor mShapeColor;
    QPen mPen;
    ShapeType mShape;

    void onShapeChanged();
    QPointF compute(float t);  // dispatch function based on mShape's type
    QPointF computeAstroid(float t);
    QPointF computeCycloid(float t);
    QPointF computeHuygens(float t);
    QPointF computeHypo(float t);
    QPointF computeLine(float t);
    QPointF computeCircle(float t);
    QPointF computeEllipse(float t);
    QPointF computeFancy(float t);
    QPointF computeStarfish(float t);
    QPointF computeCloud1(float t);
    QPointF computeCloud2(float t);
    QPointF computeCloudWithSign(float t, int sign);

    float mIntervalLength, mScale;
    int mStepCount;

    float _ellipseA, _ellipseB;
    float _fancyA, _fancyB;
    float _starfish_R, _starfish_r, _starfish_d;
};

#endif // RENDERAREA_H
