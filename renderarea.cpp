#include <QPainter>
#include "renderarea.h"
#include <math.h>

RenderArea::RenderArea(QWidget *parent) :
    QWidget{parent},
    mBackgroundColor{Qt::blue},
    // Chainging from just a QColor to QPen object.
    // mShapeColor{255, 255, 255},
    mPen{Qt::white},
    mShape{Astroid},
    _ellipseA{2.f}, _ellipseB{1.1f},
    _fancyA{11.f}, _fancyB{6.f},
    _starfish_R{5.f}, _starfish_r{3.f}, _starfish_d{5.f}
{
    mPen.setWidth(2);

    onShapeChanged();
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize{400, 400};
}

QSize RenderArea::sizeHint() const
{
    return QSize{400, 400};
}

void RenderArea::onShapeChanged() {
    switch (mShape) {
    case Astroid:
        mScale = 90;
        mIntervalLength = 2 * M_PI;
        mStepCount = 256;
        break;
    case Cycloid:
        mScale = 10;
        mIntervalLength = 4 * M_PI;
        mStepCount = 128;
        break;
    case HuygensCycloid:
        mScale = 12;
        mIntervalLength = 4 * M_PI;
        mStepCount = 256;
        break;
    case HypoCycloid:
        mScale = 40;
        mIntervalLength = 2 * M_PI;
        mStepCount = 256;
        break;
    case Line:
        mScale = 100;           // Line length in pixels
        mIntervalLength = 2;    // Not really needed for a line
        mStepCount = 128;
        break;
    case Circle:
        mScale = 165;
        mIntervalLength = 2 * M_PI;
        mStepCount = 128;
        break;
    case Ellipse:
        mScale = 75;
        mIntervalLength = 2 * M_PI;
        mStepCount = 128;
        break;
    case Fancy:
        mScale = 10;
        mIntervalLength = 12 * M_PI;
        mStepCount = 512;
        break;
    case Starfish:
        mScale = 25;
        mIntervalLength = 6 * M_PI;
        mStepCount = 256;
        break;
    case Cloud1:
        mScale = 10;
        mIntervalLength = 28 * M_PI;
        mStepCount = 128;
        break;
    case Cloud2:
        mScale = 10;
        mIntervalLength = 28 * M_PI;
        mStepCount = 128;
        break;
    default:
        break;
    }
}

QPointF RenderArea::compute(float t) {
    switch (mShape) {
    case Astroid:
        return computeAstroid(t);
        break;
    case Cycloid:
        return computeCycloid(t);
        break;
    case HuygensCycloid:
        return computeHuygens(t);
        break;
    case HypoCycloid:
        return computeHypo(t);
        break;
    case Line:
        return computeLine(t);
        break;
    case Circle:
        return computeCircle(t);
        break;
    case Ellipse:
        return computeEllipse(t);
        break;
    case Fancy:
        return computeFancy(t);
        break;
    case Starfish:
        return computeStarfish(t);
        break;
    case Cloud1:
        return computeCloud1(t);
        break;
    case Cloud2:
        return computeCloud2(t);
        break;
    default:
        break;
    }
    return QPointF{0, 0};
}

QPointF RenderArea::computeAstroid(float t)
{
    float cos_t{cos(t)};
    float sin_t{sin(t)};
    float x{float(2 * pow(cos_t, 3))};
    float y{float(2 * pow(sin_t, 3))};
    return QPointF{x, y};
}

QPointF RenderArea::computeCycloid(float t) {
    return QPointF{
                1.5 * (1 - cos(t)), // x
                1.5 * (t - sin(t))  // y
    };

}

QPointF RenderArea::computeHuygens(float t) {
    return QPointF{
                4 * (3 * cos(t) - cos(3 * t)),  // x
                4 * (3 * sin(t) - sin(3 * t))   // y
    };
}

QPointF RenderArea::computeHypo(float t) {
    return QPointF{
                1.5 * (2 * cos(t) + cos(2 * t)),    // x
                1.5 * (2 * sin(t) - sin(2 * t))     // y
    };
}

QPointF RenderArea::computeLine(float t) {
    return QPointF{1 - t, 1 - t};
}

QPointF RenderArea::computeCircle(float t) {
    return QPointF{
                cos(t), // x
                sin(t)  // y

    };
}

QPointF RenderArea::computeEllipse(float t) {
    return QPointF{
                _ellipseA * cos(t), // x
                _ellipseB * sin(t)  // y
    };
}

QPointF RenderArea::computeFancy(float t) {
    float value1{_fancyA/_fancyB};
    return QPointF{
                _fancyA * cos(t) - _fancyB * cos(value1 * t),  // x
                _fancyA * sin(t) - _fancyB * sin(value1 * t),  // y
    };
}

QPointF RenderArea::computeStarfish(float t) {
    float value1{_starfish_R - _starfish_r};
    float value2{t * (_starfish_R - _starfish_r) / _starfish_r};
    return QPointF{
                value1 * cos(t) + _starfish_d * cos(value2),   // x
                value1 * sin(t) - _starfish_d * sin(value2)    // y
    };
}

QPointF RenderArea::computeCloud1(float t) {
    return computeCloudWithSign(t, -1);
}

QPointF RenderArea::computeCloud2(float t) {
    return computeCloudWithSign(t, 1);
}

QPointF RenderArea::computeCloudWithSign(float t, int sign) {
    float a{14.f}, b{1.f};
    float x, y;

    x = (a + b) * cos(t * b / a) + sign * b * cos(t * (a +b) / a);
    y = (a + b) * sin(t * b / a) - b * sin(t * (a +b) / a);

    return QPointF{x, y};
}

void RenderArea::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event); // Get rid of the "unused variable" warnings of the compiler.

    QPainter painter{this};
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setBrush(mBackgroundColor);
    // Chainging from just a QColor to QPen object.
    //painter.setPen(mShapeColor);
    painter.setPen(mPen);

    // drawing area
    painter.drawRect(this->rect());

    QPoint center{this->rect().center()};

    QPointF prevPoint{compute(0)};
    QPointF prevPixel;
    prevPixel.setX(prevPoint.x() * mScale + center.x());
    prevPixel.setY(prevPoint.y() * mScale + center.y());

    float step{mIntervalLength / mStepCount};
    for(float t{0}; t < mIntervalLength; t += step) {
        QPointF point{compute(t)};
        QPoint pixel;
        pixel.setX(point.x() * mScale + center.x());
        pixel.setY(point.y() * mScale + center.y());

        // painter.drawPoint(pixel);    For drawing a point.
        painter.drawLine(pixel, prevPixel);
        prevPixel = pixel;
    }
    QPointF point{compute(mIntervalLength)};
    QPoint pixel;
    pixel.setX(point.x() * mScale + center.x());
    pixel.setY(point.y() * mScale + center.y());
    painter.drawLine(pixel, prevPixel);
}
